[![N|Solid](https://lh3.googleusercontent.com/hVBIcsQpnG9U7XJvc2EqvT3Xwub4AorjfnHCfFYPSF0skce9KcaJni_4FTDRv1TGLPRLKwM=s85)](https://www.linkedin.com/in/luisvasv/)
# **Luis Fernando Vásquez Vergara**
****
More than 12 years working in the software industry, high experience in data, event-oriented programming, objects, scripting, and functional.
Currently, I'm completely dedicated to the world of Big Data and analytics, designing and creating different components integrated with CDH (Cloudera Hadoop), Spark and Kafka, AWS, and others.

**A little about me :**

- Master's Student in Analytical Engineering
- I'm a Linux lover
- I'm a calm, respectful, analytical, self-taught person and a quick learner.
- I have experience as a business teacher/trainer in Linux, Python, and Big Data.
- I love programming and the world of data.

## **Programming Languages**
****
- Python
- Java
- Bash
- PL/SQL
- PowerBuilder
- Scala(currently in training).
- Swift(in the future)

## **Skills**
****
- AWS
- Spark
- Cloudera(Hadoop, Pig, Hive, Impala, Oozie, Yarn, Zookeeper)
- File formats as: 
-- JSON
-- Avro
-- Parquet
-- YAML(yml)
- Docker
- Kubernetes
- Git(CI/CD)
- Databases as:
-- Oracle
-- PostgreSQL
-- MySql
-- SQLite
-- MongoDB
- Apache Kafka
- Apache Airflow
- SQL
- Others

## **Certifications**
****
- AI For all
- Introduction to Data Science in Python
- MTA: Introduction to Programming Using Python
- iZ0-146 Oracle Database 11g: Advanced PL/SQL
- Oracle PL/SQL Developer Certified Associate
- Oracle Database SQL Expert 1Z0-047
- Information management with BIG DATA
- Apache Spark Streaming with Python and PySpark
- Sixty years of artificial intelligence

