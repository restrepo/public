
from jsonschema import validate, FormatChecker
import json
import logging
import jsonschema 
import yaml

def schema_validation(schema, document):  
    response = {
        "code": 0,
        "msm": ""
    }
    try:
        validate(
            schema=schema,
            instance=document,
            format_checker=FormatChecker()
        )
        response["msm"] = "the schama is valid"
    except Exception as ex:
        response["code"] = -1
        response["msm"] = str(ex)
    return response

def read_json_file(path: str):
    data = {}
    try:
        with open(path) as lfile:
            data = json.load(lfile)
    except jsonschema.exceptions.ValidationError as ex:
        get_basic_logging().error("problems detected reading JSON file: {}".format(str(ex)))
        exit(1)
    return data

def get_basic_logging():
    logger = logging.getLogger('python-medellin')
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s] ---> %(message)s', datefmt='[%Y/%m/%d %H:%M:%S]'
    )
    return logger

def get_schema_from_yaml(schema: str):
    config_yaml = {}
    try:
        with open(schema) as file:
            config_yaml = yaml.load(file, Loader=yaml.FullLoader)
    except Exception:
        config_yaml = None
    return config_yaml

