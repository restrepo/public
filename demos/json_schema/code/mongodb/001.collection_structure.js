use demo;
db.createCollection(
   "demo-schema-json",
   {
      "validator":{
         "$jsonSchema":{   
            "title": "JSON Schema Validation",
            "description": "Demo : working with json schemas",
            "bsonType": "object",
            "additionalProperties": false,
            "properties":{
                "_id":{
                    "bsonType": "objectId"
                },
                "user":{
                    "description": "user event",
                    "bsonType": "string"
                },
                "events": {
                    "description": "events that the user did into the app",
                    "bsonType": "array",
                    "minItems" :1,
                    "items":{
                        "bsonType": "object",
                        "additionalProperties": false,
                        "properties":{
                            "date_event": {
                                "description": "event date",
                                "bsonType":"string"
                            },
                            "event":{
                                "description": "event description or audit actions",
                                "bsonType": "string",
                                "minLength": 5,
                                "maxLength": 1000
                            },
                            "event_type":{
                                "description": "log event bsonType",
                                "bsonType": "string",
                                "enum": ["warning","error","info"]
                            }
                        },
                        "required":[
                            "date_event",
                            "event",
                            "event_type"
                        ]
                    }
                }
            },
            "required":[
                "user",
                "events"
            ]
        }
      }
   }
);